function eCheckValidate(){
  var res = 0;

  // valid name
  function validateName(name) {
      filter = /^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$/;
      if (!(filter.test(name))) {
          return false;
      }
      else {
          return true;
      }
  }

  // validate first name
  if ($.trim($("#name").val()) == '') {
      $("#errorName").html('Please enter first name');
      $("#name").addClass("error");
      if (res == 0) {
          res = res + 1;
      }
  }
  else {
      if (!validateName($.trim($("#name").val()))) {
          $("#errorName").html('First Name cannot be a number');
          $("#name").addClass("error");
          if (res == 0) {
              res = res + 1;
          }
      }
      else {
          $("#errorName").html('');
          $("#name").removeClass("error");
      }
  }

  // validate last name
  if ($.trim($("#last_name").val()) == '') {
      $("#errorLastName").html('Please enter last name');
      $("#last_name").addClass("error");
      if (res == 0) {
          res = res + 1;
      }
  }
  else {
      if (!validateName($.trim($("#last_name").val()))) {
          $("#errorLastName").html('Last Name cannot be a number');
          $("#last_name").addClass("error");
          if (res == 0) {
              res = res + 1;
          }
      }
      else {
          $("#errorLastName").html('');
          $("#last_name").removeClass("error");
      }
  }

  // validate rounting # //
  if ($.trim($("#s_routing").val()) == '') {
      $("#errorS_routing").html('Please enter Routing #');
      $("#s_routing").addClass("error");
      if (res == 0) {
          res = res + 1;
      }
  }

  else {
      if (validateName($.trim($("#s_routing").val())) ) {
          $("#errorS_routing").html('Routing# must be an integer');
          $("#s_routing").addClass("error");
          if (res == 0) {
              res = res + 1;
          }
      }
      else if ($.trim($("#s_routing").val()).length != 9  ) {
        $("#errorS_routing").html('Routing# must be 9 digits');
        $("#s_routing").addClass("error");
        if (res == 0) {
            res = res + 1;
        }
      }
      else if($.trim($("#s_routing").val()) > 360000000){
        $("#errorS_routing").html('Routing# must be less than 360 000 000');
        $("#s_routing").addClass("error");
        if (res == 0) {
            res = res + 1;
        }
      }
      else if($.trim($("#s_routing").val()).length == 9 && isNaN($.trim($("#s_routing").val()))){
        $("#errorS_routing").html('Routing# must be an integer');
        $("#s_routing").addClass("error");
        if (res == 0) {
            res = res + 1;
        }
      }
      else {
          $("#errorS_routing").html('');
          $("#s_routing").removeClass("error");
      }

  }

  // validate account number //
  if ($.trim($("#s_account").val()) == '') {
      $("#errorS_account").html('Please enter account #');
      $("#s_account").addClass("error");
      if (res == 0) {
          res = res + 1;
      }
  }
  else {
      if (isNaN($.trim($("#s_account").val()))) {
          $("#errorS_account").html('Account # must be an integer');
          $("#s_account").addClass("error");
          if (res == 0) {
              res = res + 1;
          }
      }
      else {
          $("#errorS_account").html('');
          $("#s_account").removeClass("error");
      }
  }

  // validate address 1 //
  if ($.trim($("#s_address1").val()) == '') {
      $("#errorS_address1").html('Please enter address 1');
      $("#s_address1").addClass("error");
      if (res == 0) {
          res = res + 1;
      }
  }
   else {
          $("#errorS_address1").html('');
          $("#s_address1").removeClass("error");
      }

  // validate address 2 //
  if ($.trim($("#s_address2").val()) == '') {
      $("#errorS_address2").html('Please enter address 2');
      $("#s_address2").addClass("error");
      if (res == 0) {
          res = res + 1;
      }
  }
   else {
          $("#errorS_address2").html('');
          $("#s_address2").removeClass("error");
      }

  // validate city //
  if ($.trim($("#s_city").val()) == '') {
      $("#errorS_city").html('Please enter your city');
      $("#s_city").addClass("error");
      if (res == 0) {
          res = res + 1;
      }
  }
   else {
          $("#errorS_city").html('');
          $("#s_city").removeClass("error");
      }
  // validate memo //
  if ($.trim($("#memo").val()) == '') {
      $("#errorMemo").html('Please write memo');
      $("#memo").addClass("error");
      if (res == 0) {
          res = res + 1;
      }
  }
   else {
          $("#errorMemo").html('');
          $("#memo").removeClass("error");
      }

  // validate state //
  if ($.trim($("select[name=state]").val()) == '') {
      $("#errorState").html('Please select a state');
      $("#state").addClass("error");
      if (res == 0) {
          res = res + 1;
      }
  }
   else {
          $("#errorState").html('');
          $("#state").removeClass("error");
      }
  // validate zip colde //
  if ($.trim($("#s_zip").val()) == '') {
      $("#errorS_zip").html('Please enter zip code');
      $("#s_zip").addClass("error");
      if (res == 0) {
          res = res + 1;
      }
  }
   else {
          $("#errorS_zip").html('');
          $("#s_zip").removeClass("error");
      }

  // validate amount //
  if ($.trim($("#s_amount").val()) == '') {
      $("#errorS_amount").html('Please enter amount');
      $("#s_amount").addClass("error");
      if (res == 0) {
          res = res + 1;
      }
  }
  else {
      if (isNaN($.trim($("#s_amount").val()))) {
          $("#errorS_amount").html('Amount must be an integer');
          $("#s_amount").addClass("error");
          if (res == 0) {
              res = res + 1;
          }
      }
      else {
          $("#errorS_amount").html('');
          $("#s_amount").removeClass("error");
      }
  }


  if(res == 1){
    return false;
  }
  else {
    return true;
  }
}
