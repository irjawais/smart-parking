function formatDate(date) {
    return moment(date).format("MM-DD-YYYY")
}

function formatDateTime(date) {
    return moment(date).format("MM-DD-YYYY HH:mm")
}