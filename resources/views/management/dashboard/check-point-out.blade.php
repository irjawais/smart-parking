@extends('layouts.app_management')
@section("content")

<div class="main" style="min-height: 100px;">

        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <!-- OVERVIEW -->
                <div class="panel panel-headline">
                    <div class="panel-heading">
                        <h3 class="panel-title">Check Point Out</h3>
                        <p class="panel-subtitle">Menual And Automatic Check Point Out</p>
                    </div>
                    <div class="panel-body">

                    </div>
                </div>
                <!-- END OVERVIEW -->

                @include('management.dashboard.loader')


                <div class="row">
                    <div class="col-md-6">
                        <!-- RECENT PURCHASES -->
                        <div class="panel">

                                <div class="panel">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Check Point  Out</h3>
                                        </div>
                                        <div class="panel-body">
                                            <form method="post" id="check-point-form" >
                                            <div class="input-group">
                                                <input class="form-control"  id="qr_code"placeholder="Card QR Code #" type="password">
                                                <span class="input-group-btn"> </span>
                                            </div><br>
                                            </form>
                                        </div>
                                    </div>
                        </div>
                        <!-- END RECENT PURCHASES -->

                    </div>

                </div>


            </div>
        </div>
        <!-- END MAIN CONTENT -->

    </div>
    @endsection
    @section("script")
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script>
                        $('.loader-container').hide()

       openalpr =  '{{ENV('openalpr')}}';
       $('#check-point-form').on('submit',function (e) {
            $('.loader-container').show()
            e.preventDefault();

            var qr_code = $('#qr_code').val()
            console.log(qr_code)
            $('#qr_code').val("")
                verify_user(qr_code)
            return true;
            /// capture image and store in vehicle_images folders code here
            var imgUrl  = '{{ asset('vehicle_images/vehicle1.jpg') }}';
            var plate_data ;
            var plate ;
            let base64image = getBase64Image(imgUrl).then(function(base64image) {
                $.ajax({
                    url: "https://api.openalpr.com/v2/recognize_bytes?recognize_vehicle=1&country=us&secret_key=" + openalpr,
                    type: "post",
                    data: base64image ,
                    success: function (response) {
                        try {
                            plate_data = response;
                            if(plate_data.results &&  plate_data.results[0] && plate_data.results[0].plate){
                                plate = plate_data.results[0].plate;
                                console.log(`Request to server with ${plate,qr_code}`)
                                //verify_user(plate,qr_code)
                            }else{

                            }
                            console.log(plate);
                        }
                        catch(err) {
                            document.getElementById("demo").innerHTML = err.message;
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
            }, function(reason) {
                console.log(reason); // Error!
            });


        })

        function getBase64Image(imgUrl) {
        return new Promise(
            function(resolve, reject) {

            var img = new Image();
            img.src = imgUrl;
            img.setAttribute('crossOrigin', 'anonymous');

            img.onload = function() {
                var canvas = document.createElement("canvas");
                canvas.width = img.width;
                canvas.height = img.height;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0);
                var dataURL = canvas.toDataURL("image/png");
                resolve(dataURL.replace(/^data:image\/(png|jpg);base64,/, ""));
            }
            img.onerror = function() {
                reject("The image could not be loaded.");
            }

            });

        }
        function verify_user(qr_code){
            /* plate = plate.replace(/[^a-zA-Z ]/g, "");
            plate.replace(/\s/g, '') */
            $.ajax({
               type:'POST',
               url:'{{route("management.ajaxVerifyUserOut")}}',
               data:{
                   "_token": "{{ csrf_token() }}",
                    "qr_code":qr_code
                   },
               success:function(data) {
                console.log(data)
                if(data.status =="success"){
                    swal("Good job!", data.message, "success");

                }else{

                    swal("Failed!", data.message, "error");

                }
                $('.loader-container').hide()

               }
            });
        }

    </script>
    @endsection




