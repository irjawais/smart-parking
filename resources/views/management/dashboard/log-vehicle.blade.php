<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$config['name']}}</title>
    <meta name="description" content="{{$config['name']}}" />
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" type="text/css" href="{{asset("CSS3ButtonSwitches/css/style.css")}}" />
    <link rel="stylesheet" type="text/css" href="{{asset("CSS3ButtonSwitches/css/font-awesome.css")}}" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:700,300' rel='stylesheet' type='text/css' />

</head>

<body>
    <div class="container">

        <center>
        <h1 style="color: gray;">Smart Parking System! {{$config['name']}}</h1>
            <h1 style="color: gray;"> Click here to log vehicle!</h1>
            <a style="opacity: 0.1" id="link" href="{{route('management.entryPoint',1)}}">Link</a>
        </center>
        <section class="main" style="margin-top: 25vh;">



            <div class="switch demo4">
                <input id="checkbox" type="checkbox" checked>
                <label><i class='icon-off'></i></label>
            </div>

        </section>

    </div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

    <script>
        var anchor = document.getElementById('link');
        var link = anchor.getAttribute('href');

        $('#checkbox').click(function(){
            console.log("here")
            window.open(link, '_blank');

        });
    </script>
</body>


</html>
