@extends('layouts.app_management')
@section("content")


<div class="main" style="min-height: 100px;">

        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <!-- OVERVIEW -->
                {{-- <div class="panel panel-headline">
                    <div class="panel-heading">
                        <h3 class="panel-title">Check Point</h3>
                        <p class="panel-subtitle">Menual And Automatic Check Point</p>
                    </div>
                    <div class="panel-body">

                    </div>
                </div> --}}
                <!-- END OVERVIEW -->
                @include('management.dashboard.loader')

                <div class="row">


                    <div class="col-md-6">
                        <!-- RECENT PURCHASES -->
                        <div class="panel hide-this">

                                <div class="panel ">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Camera</h3>
                                        </div>
                                        <div class="panel-body">
                                        	<div id="my_camera"></div>

                                        </div>
                                    </div>
                        </div>
                        <!-- END RECENT PURCHASES -->
                    </div>

                    <div class="col-md-6">
                            <!-- RECENT PURCHASES -->
                            <div class="panel">

                                    <div class="panel">
                                            <div class="panel-heading">
                                           {{--      <h3 class="panel-title">Card</h3> --}}
                                            </div>
                                            <div class="panel-body">
                                                    <div class="id-card-holder" id="my-node">
                                                            <div class="id-card">
                                                                <div class="header">
                                                                    <img src="{{@$config['logo1']}}">
                                                                </div>
                                                                {{-- <div class="photo">
                                                                    <img src="https://krooqi.com/wp-content/uploads/2018/12/user-profile-male-logo.jpg">
                                                                </div> --}}
                                                            <h2 id="card_name"> </h2>
                                                            <img src='' id='mp_qr'  style=" display: block; margin: auto; width:80%; " >
                                                                <div class="qr-code" style="margin: 0 auto;">
                                                                    {{-- <img style=" width: 25vh;" src='https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl={{Auth::user()->id}}&choe=UTF-8'  /> --}}
                                                             </div>

                                                                <hr>
                                                                <p><strong>{{$config['name']}}</strong>{{$config['location']}}</p>


                                                            </div>
                                                        </div>

                                            </div>
                                        </div>
                            </div>
                            <!-- END RECENT PURCHASES -->
                        </div>

                </div>



            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>


    <style>

            body {
                        background-color: #d7d6d3;
                        font-family:'verdana';
                    }
                    .id-card-holder {
                        width: 225px;
                        padding: 4px;
                        margin: 0 auto;
                        background-color: #1f1f1f;
                        border-radius: 5px;
                        position: relative;
                    }
                    .id-card-holder:after {
                        content: '';
                        width: 7px;
                        display: block;
                        background-color: #0a0a0a;
                        height: 100px;
                        position: absolute;
                        top: 105px;
                        border-radius: 0 5px 5px 0;
                    }
                    .id-card-holder:before {
                        content: '';
                        width: 7px;
                        display: block;
                        background-color: #0a0a0a;
                        height: 100px;
                        position: absolute;
                        top: 105px;
                        left: 222px;
                        border-radius: 5px 0 0 5px;
                    }
                    .id-card {

                        background-color: #fff;
                        padding: 10px;
                        border-radius: 10px;
                        text-align: center;
                        box-shadow: 0 0 1.5px 0px #b9b9b9;
                    }
                    .id-card img {
                        margin: 0 auto;
                    }
                    .header img {
                        width: 100px;
                        margin-top: 15px;
                    }
                    .photo img {
                        width: 80px;
                        margin-top: 15px;
                    }
                    h2 {
                        font-size: 15px;
                        margin: 5px 0;
                    }
                    h3 {
                        font-size: 12px;
                        margin: 2.5px 0;
                        font-weight: 300;
                    }
                    .qr-code img {
                        width: 50px;
                    }
                    p {
                        font-size: 6px;
                        margin: 0vh;
                    }
                    .id-card-hook {
                        background-color: #000;
                        width: 70px;
                        margin: 0 auto;
                        height: 15px;
                        border-radius: 5px 5px 0 0;
                    }
                    .id-card-hook:after {
                        content: '';
                        background-color: #d7d6d3;
                        width: 47px;
                        height: 6px;
                        display: block;
                        margin: 0px auto;
                        position: relative;
                        top: 6px;
                        border-radius: 4px;
                    }
                    .id-card-tag-strip {
                        width: 45px;
                        height: 40px;
                        background-color: #0950ef;
                        margin: 0 auto;
                        border-radius: 5px;
                        position: relative;
                        top: 9px;
                        z-index: 1;
                        border: 1px solid #0041ad;
                    }
                    .id-card-tag-strip:after {
                        content: '';
                        display: block;
                        width: 100%;
                        height: 1px;
                        background-color: #c1c1c1;
                        position: relative;
                        top: 10px;
                    }
                    .id-card-tag {
                        width: 0;
                        height: 0;
                        border-left: 100px solid transparent;
                        border-right: 100px solid transparent;
                        border-top: 100px solid #0958db;
                        margin: -10px auto -30px auto;
                    }
                    .id-card-tag:after {
                        content: '';
                        display: block;
                        width: 0;
                        height: 0;
                        border-left: 50px solid transparent;
                        border-right: 50px solid transparent;
                        border-top: 100px solid #d7d6d3;
                        margin: -10px auto -30px auto;
                        position: relative;
                        top: -130px;
                        left: -50px;
                    }
                    @page {
                        size:A4 landscape;
                        margin-left: 0px;
                        margin-right: 0px;
                        margin-top: 0px;
                        margin-bottom: 0px;
                        margin: 0;
                        -webkit-print-color-adjust: exact;
                    }
                    @media print {


                        .main-panel{
                          //  background-color: white;
                        }
                        .hide-this, panel, .footer, #printBtn,button,.nav-link{
                            display: none;
                        }
                        -webkit-print-color-adjust: exact !important; /*Chrome, Safari */
                        color-adjust: exact !important;  /*Firefox*/

                        .id-card-holder {
                        width: 400px;
                        padding: 4px;
                        margin: 0 auto;
                        background-color: #1f1f1f;
                        border-radius: 5px;
                        position: relative;
                    }
                    .id-card-holder:after {
                        content: '';
                        width: 7px;
                        display: block;
                        background-color: #0a0a0a;
                        height: 100px;
                        position: absolute;
                        top: 105px;
                        border-radius: 0 5px 5px 0;
                    }
                    .id-card-holder:before {
                        content: '';
                        width: 7px;
                        display: block;
                        background-color: #0a0a0a;
                        height: 100px;
                        position: absolute;
                        top: 105px;
                        left: 222px;
                        border-radius: 5px 0 0 5px;
                    }
                    .id-card {

                        background-color: #fff;
                        padding: 10px;
                        border-radius: 10px;
                        text-align: center;
                        box-shadow: 0 0 1.5px 0px #b9b9b9;
                    }
                    .id-card img {
                        margin: 0 auto;
                    }
                    .header img {
                        width: 100px;
                        margin-top: 15px;
                    }
                    .photo img {
                        width: 80px;
                        margin-top: 15px;
                    }
                    h2 {
                        font-size: 15px;
                        margin: 5px 0;
                    }
                    h3 {
                        font-size: 12px;
                        margin: 2.5px 0;
                        font-weight: 300;
                    }
                    .qr-code img {
                        width: 50px;
                    }
                    p {
                        font-size: 6px;
                         margin: 0vh;
                    }
                    .id-card-hook {
                        background-color: #000;
                        width: 70px;
                        margin: 0 auto;
                        height: 15px;
                        border-radius: 5px 5px 0 0;
                    }
                    .id-card-hook:after {
                        content: '';
                        background-color: #d7d6d3;
                        width: 47px;
                        height: 6px;
                        display: block;
                        margin: 0px auto;
                        position: relative;
                        top: 6px;
                        border-radius: 4px;
                    }
                    .id-card-tag-strip {
                        width: 45px;
                        height: 40px;
                        background-color: #0950ef;
                        margin: 0 auto;
                        border-radius: 5px;
                        position: relative;
                        top: 9px;
                        z-index: 1;
                        border: 1px solid #0041ad;
                    }
                    .id-card-tag-strip:after {
                        content: '';
                        display: block;
                        width: 100%;
                        height: 1px;
                        background-color: #c1c1c1;
                        position: relative;
                        top: 10px;
                    }
                    .id-card-tag {
                        width: 0;
                        height: 0;
                        border-left: 100px solid transparent;
                        border-right: 100px solid transparent;
                        border-top: 100px solid #0958db;
                        margin: -10px auto -30px auto;
                    }
                    .id-card-tag:after {
                        content: '';
                        display: block;
                        width: 0;
                        height: 0;
                        border-left: 50px solid transparent;
                        border-right: 50px solid transparent;
                        border-top: 100px solid #d7d6d3;
                        margin: -10px auto -30px auto;
                        position: relative;
                        top: -130px;
                        left: -50px;
                    }
                    .qr-code,img {margin: 0px auto; padding: 0px; overflow: auto; width: 139px;}
                    .id-card {
                            background-color: #fff;
                            padding: 10px;
                            border-radius: 10px;
                            text-align: center;
                            box-shadow: 0 0 1.5px 0px #b9b9b9;
                        }
                        .id-card-holder {
                            width: 225px;
                            padding: 4px;
                            margin: 0 auto;
                            background-color: #1f1f1f;
                            border-radius: 5px;
                            position: relative;
                        }
                    }
            </style>

    @endsection
    @section("script")
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


	<script src="https://pixlcore.com/demos/webcamjs/webcam.min.js"></script>
    <script type="text/javascript"  src="/js/jquery-barcode.min.js" ></script>

    <script>
        $('document').ready(function(){
            /* swal("Enter Vehicle Number!", {
                                    content: "input",
                })
                .then((value) => { */
                   /*  var number_plate = value;
                    if(!number_plate)
                        return false; */
                    number_plate = false;
                    setTimeout(function(){
                        var base64image = take_snapshot();
                        console.log(base64image)
                        save_record(number_plate,base64image)
                    },2000);



             /*    }); */

        });

        function save_record(number_plate,base64image){
            /* plate = plate.replace(/[^a-zA-Z ]/g, "");
            plate.replace(/\s/g, '') */
            $.ajax({
               type:'POST',
               url:'{{route("management.ajaxSaveVehicle")}}',
               data:{
                   "_token": "{{ csrf_token() }}",
                   "vehicle_type_id": "{{ $vehicle_type }}",

                    "number_plate":number_plate,
                    "base64image":base64image
                   },
               success:function(data) {
                console.log(data)

                $('.loader-container').hide()

                if(data.status == "error"){
                    swal("Failed!", data.message, "error");
                }
                else if(data.status =="success"){
                    $('#card_name').text(data.message)
                    $("#mp_qr").attr("src","");

                    console.log(data.url);
                    //console.log(data.map);
                    $("#mp_qr").attr("src",data.url);

                    $(".qr-code").barcode(
                        data.generated_id,
                        "ean13"
                    );
                    window.print();
                    //swal("Good job!", data.message, "success");
                }
                else
                    alert("some thing went wrong");
               }
            });
        }

		Webcam.set({
			width: 320,
			height: 240,
			dest_width: 640,
			dest_height: 480,
			image_format: 'jpeg',
			jpeg_quality: 90
		});
		Webcam.attach( '#my_camera' );

        function take_snapshot() {
            data_uri = false;
            // take snapshot and get image data
             Webcam.snap( function(_data_uri) {
                 data_uri = _data_uri;
            } );
            return data_uri;
        }
    </script>

  <script>
        $('.loader-container').hide()

       openalpr =  '{{ENV('openalpr')}}';
        //function checkPoint(){
        $('#check-point-form').on('submit',function (e) {
            e.preventDefault();
            $('.loader-container').show()

            var qr_code = $('#qr_code').val()
            console.log(qr_code)
            $('#qr_code').val("")
            /// capture image and store in vehicle_images folders code here
            var imgUrl  = '{{ asset('vehicle_images/vehicle1.jpg') }}';
            var plate_data ;
            var plate ;
            let base64image = getBase64Image(imgUrl).then(function(base64image) {
              //  base64image = take_snapshot();
                console.log(base64image)
                $.ajax({
                    url: "https://api.openalpr.com/v2/recognize_bytes?recognize_vehicle=1&country=us&secret_key=" + openalpr,
                    type: "post",
                    data: base64image ,
                    success: function (response) {
                        try {
                            plate_data = response;
                            if(plate_data.results &&  plate_data.results[0] && plate_data.results[0].plate){
                                plate = plate_data.results[0].plate;
                                console.log(`Request to server with ${plate,qr_code}`)
                                verify_user(plate,qr_code)
                            }else{

                                swal("Number plate reading failed!  Enter plate #!", {
                                    content: "input",
                                })
                                .then((value) => {
                                    verify_user(value,qr_code)
                                });
                                $('.loader-container').hide()

                            }
                            console.log(plate);
                        }
                        catch(err) {
                            console.log(err)
                            /* swal("Number plate reading failed!  Enter plate #!", {
                                    content: "input",
                                })
                                .then((value) => {
                                    verify_user(value,qr_code)
                                }); */
                                $('.loader-container').hide()

                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $('.loader-container').hide()
                        swal("System need some training!")
                        /* swal("Number plate reading failed!  Enter plate #!", {
                                    content: "input",
                                })
                                .then((value) => {
                                    verify_user(value,qr_code)
                                });
                                $('.loader-container').hide() */

                    }
                });
            }, function(reason) {
                console.log(reason); // Error!
            });


        })

        function getBase64Image(imgUrl) {
        return new Promise(
            function(resolve, reject) {

            var img = new Image();
            img.src = imgUrl;
            img.setAttribute('crossOrigin', 'anonymous');

            img.onload = function() {
                var canvas = document.createElement("canvas");
                canvas.width = img.width;
                canvas.height = img.height;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0);
                var dataURL = canvas.toDataURL("image/png");
                resolve(dataURL.replace(/^data:image\/(png|jpg);base64,/, ""));
            }
            img.onerror = function() {
                reject("The image could not be loaded.");
            }

            });

        }
        function verify_user(plate,qr_code){
            /* plate = plate.replace(/[^a-zA-Z ]/g, "");
            plate.replace(/\s/g, '') */
            $.ajax({
               type:'POST',
               url:'{{route("management.ajaxVerifyUser")}}',
               data:{
                   "_token": "{{ csrf_token() }}",
                    "plate":plate,
                    "qr_code":qr_code
                   },
               success:function(data) {
                console.log(data)
                $('.loader-container').hide()

                if(
                    data.status == "error-user" ||
                     data.status =="error-location-user-type-vehicle" ||
                     data.status =="error-balance" ||
                     data.status =="error-slots" ||
                     data.status =="error-parking" ||
                     data.status =="error-plate-null"

                ){
                    swal("Failed!", data.message, "error");
                }
                else if(data.status =="error-vehicle"){
                    swal("Number plate reading failed!  Enter plate #!", {
                        content: "input",
                    })
                    .then((value) => {
                        verify_user(value,qr_code)
                    });
                }
                else if(data.status =="success"){
                    swal("Good job!", data.message, "success");

                }
                else
                    alert("some thing went wrong");
               }
            });
        }


    </script>
    @endsection




