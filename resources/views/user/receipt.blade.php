@extends('layouts.apps')

@section('content')


    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">

                <div class="id-card-holder" id="my-node">
                        <div class="id-card">
                            <div class="header">
                                <img src="{{@$config['logo1']}}">
                            </div>
                            <div class="photo">
                                <img src="https://krooqi.com/wp-content/uploads/2018/12/user-profile-male-logo.jpg">
                            </div>
                        <h2 id="card_name">{{Auth::user()->name}}</h2>
                            <div class="qr-code" style="margin: 0 auto;">
                                {{-- <img style=" width: 25vh;" src='https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl={{Auth::user()->id}}&choe=UTF-8'  /> --}}
                         </div>
                            <h3>{{@route("main")}}</h3>
                            <hr>
                            <p><strong>{{$config['name']}}</strong>{{$config['location']}}</p>


                        </div>
                    </div>
                <button class="btn btn-primary " id="printBtn">Download or Print Card<div class="ripple-container"></div></button>

          </div>

          {{-- <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
              <div class="card-header card-header-danger card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">info_outline</i>
                </div>
                <p class="card-category">Fixed Issues</p>
                <h3 class="card-title">75</h3>
              </div>
              <div class="card-footer">
                <div class="stats">
                  <i class="material-icons">local_offer</i> Tracked from Github
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
              <div class="card-header card-header-info card-header-icon">
                <div class="card-icon">
                  <i class="fa fa-twitter"></i>
                </div>
                <p class="card-category">Followers</p>
                <h3 class="card-title">+245</h3>
              </div>
              <div class="card-footer">
                <div class="stats">
                  <i class="material-icons">update</i> Just Updated
                </div>
              </div>
            </div>
          </div> --}}
        </div>

      </div>
    </div>
    <style>

            body {
                        background-color: #d7d6d3;
                        font-family:'verdana';
                    }
                    .id-card-holder {
                        width: 225px;
                        padding: 4px;
                        margin: 0 auto;
                        background-color: #1f1f1f;
                        border-radius: 5px;
                        position: relative;
                    }
                    .id-card-holder:after {
                        content: '';
                        width: 7px;
                        display: block;
                        background-color: #0a0a0a;
                        height: 100px;
                        position: absolute;
                        top: 105px;
                        border-radius: 0 5px 5px 0;
                    }
                    .id-card-holder:before {
                        content: '';
                        width: 7px;
                        display: block;
                        background-color: #0a0a0a;
                        height: 100px;
                        position: absolute;
                        top: 105px;
                        left: 222px;
                        border-radius: 5px 0 0 5px;
                    }
                    .id-card {

                        background-color: #fff;
                        padding: 10px;
                        border-radius: 10px;
                        text-align: center;
                        box-shadow: 0 0 1.5px 0px #b9b9b9;
                    }
                    .id-card img {
                        margin: 0 auto;
                    }
                    .header img {
                        width: 100px;
                        margin-top: 15px;
                    }
                    .photo img {
                        width: 80px;
                        margin-top: 15px;
                    }
                    h2 {
                        font-size: 15px;
                        margin: 5px 0;
                    }
                    h3 {
                        font-size: 12px;
                        margin: 2.5px 0;
                        font-weight: 300;
                    }
                    .qr-code img {
                        width: 50px;
                    }
                    p {
                        font-size: 6px;
                        margin: 0vh;
                    }
                    .id-card-hook {
                        background-color: #000;
                        width: 70px;
                        margin: 0 auto;
                        height: 15px;
                        border-radius: 5px 5px 0 0;
                    }
                    .id-card-hook:after {
                        content: '';
                        background-color: #d7d6d3;
                        width: 47px;
                        height: 6px;
                        display: block;
                        margin: 0px auto;
                        position: relative;
                        top: 6px;
                        border-radius: 4px;
                    }
                    .id-card-tag-strip {
                        width: 45px;
                        height: 40px;
                        background-color: #0950ef;
                        margin: 0 auto;
                        border-radius: 5px;
                        position: relative;
                        top: 9px;
                        z-index: 1;
                        border: 1px solid #0041ad;
                    }
                    .id-card-tag-strip:after {
                        content: '';
                        display: block;
                        width: 100%;
                        height: 1px;
                        background-color: #c1c1c1;
                        position: relative;
                        top: 10px;
                    }
                    .id-card-tag {
                        width: 0;
                        height: 0;
                        border-left: 100px solid transparent;
                        border-right: 100px solid transparent;
                        border-top: 100px solid #0958db;
                        margin: -10px auto -30px auto;
                    }
                    .id-card-tag:after {
                        content: '';
                        display: block;
                        width: 0;
                        height: 0;
                        border-left: 50px solid transparent;
                        border-right: 50px solid transparent;
                        border-top: 100px solid #d7d6d3;
                        margin: -10px auto -30px auto;
                        position: relative;
                        top: -130px;
                        left: -50px;
                    }
                    @page {
    size:A4 landscape;
    margin-left: 0px;
    margin-right: 0px;
    margin-top: 0px;
    margin-bottom: 0px;
    margin: 0;
    -webkit-print-color-adjust: exact;
}
                    @media print {

                        .main-panel{
                            background-color: white;
                        }
                        .footer, #printBtn,button,.nav-link{
                            display: none;
                        }
                        -webkit-print-color-adjust: exact !important; /*Chrome, Safari */
        color-adjust: exact !important;  /*Firefox*/

                        .id-card-holder {
                        width: 225px;
                        padding: 4px;
                        margin: 0 auto;
                        background-color: #1f1f1f;
                        border-radius: 5px;
                        position: relative;
                    }
                    .id-card-holder:after {
                        content: '';
                        width: 7px;
                        display: block;
                        background-color: #0a0a0a;
                        height: 100px;
                        position: absolute;
                        top: 105px;
                        border-radius: 0 5px 5px 0;
                    }
                    .id-card-holder:before {
                        content: '';
                        width: 7px;
                        display: block;
                        background-color: #0a0a0a;
                        height: 100px;
                        position: absolute;
                        top: 105px;
                        left: 222px;
                        border-radius: 5px 0 0 5px;
                    }
                    .id-card {

                        background-color: #fff;
                        padding: 10px;
                        border-radius: 10px;
                        text-align: center;
                        box-shadow: 0 0 1.5px 0px #b9b9b9;
                    }
                    .id-card img {
                        margin: 0 auto;
                    }
                    .header img {
                        width: 100px;
                        margin-top: 15px;
                    }
                    .photo img {
                        width: 80px;
                        margin-top: 15px;
                    }
                    h2 {
                        font-size: 15px;
                        margin: 5px 0;
                    }
                    h3 {
                        font-size: 12px;
                        margin: 2.5px 0;
                        font-weight: 300;
                    }
                    .qr-code img {
                        width: 50px;
                    }
                    p {
                        font-size: 6px;
    margin: 0vh;
                    }
                    .id-card-hook {
                        background-color: #000;
                        width: 70px;
                        margin: 0 auto;
                        height: 15px;
                        border-radius: 5px 5px 0 0;
                    }
                    .id-card-hook:after {
                        content: '';
                        background-color: #d7d6d3;
                        width: 47px;
                        height: 6px;
                        display: block;
                        margin: 0px auto;
                        position: relative;
                        top: 6px;
                        border-radius: 4px;
                    }
                    .id-card-tag-strip {
                        width: 45px;
                        height: 40px;
                        background-color: #0950ef;
                        margin: 0 auto;
                        border-radius: 5px;
                        position: relative;
                        top: 9px;
                        z-index: 1;
                        border: 1px solid #0041ad;
                    }
                    .id-card-tag-strip:after {
                        content: '';
                        display: block;
                        width: 100%;
                        height: 1px;
                        background-color: #c1c1c1;
                        position: relative;
                        top: 10px;
                    }
                    .id-card-tag {
                        width: 0;
                        height: 0;
                        border-left: 100px solid transparent;
                        border-right: 100px solid transparent;
                        border-top: 100px solid #0958db;
                        margin: -10px auto -30px auto;
                    }
                    .id-card-tag:after {
                        content: '';
                        display: block;
                        width: 0;
                        height: 0;
                        border-left: 50px solid transparent;
                        border-right: 50px solid transparent;
                        border-top: 100px solid #d7d6d3;
                        margin: -10px auto -30px auto;
                        position: relative;
                        top: -130px;
                        left: -50px;
                    }
                    .qr-code {margin: 0px auto; padding: 0px; overflow: auto; width: 139px;}
                    }
            </style>
@endsection
@section("script")

<link href="https://printjs-4de6.kxcdn.com/print.min.css" rel="stylesheet" />
<script src="  https://printjs-4de6.kxcdn.com/print.min.js"></script>

<script type="text/javascript" src="https://jasonday.github.io/printThis/printThis.js"></script>

<script type="text/javascript"  src="../js/jquery-barcode.min.js" ></script>


<script>

    $(".qr-code").barcode(
        "{{Auth::user()->generated_id}}",
        "ean13"
    );

$('#printBtn').click(function(){
    window.print();

/* var divToPrint = document.getElementById('my-node');
//var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

newWin.document.open();

newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

newWin.document.close();

setTimeout(function(){newWin.close();},10);
 */
});
</script>


@endsection



