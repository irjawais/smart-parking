@extends('layouts.apps')

@section('content')


    <div class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                  <div class="card-header card-header-primary">
                    <h4 class="card-title ">Number Plates</h4>
                    <p class="card-category">Number Plates</p>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table">
                        <thead class=" text-primary">
                                <th>
                                     Vehicle
                                </th>
                            <th>
                            Number Plate ID
                          </th>
                          <th>
                            QR code
                          </th>
                          <th>
                            Download
                          </th>

                        </thead>
                        <tbody>

                            @foreach ($vehicles as $vehicle)
                            <tr>
                                    <td>
                                      {{ $vehicle->parent->name}}
                                    </td>
                                    <td>
                                        {{$vehicle->vehicle_number}}
                                    </td>
                                    <td>
                                    <img style=" width: 25vh;" class="qrcode{{ $vehicle->id}}" src='https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl={{$vehicle->id}}&choe=UTF-8'  />

                                    </td>
                                    <td>
                                            <button class="btn btn-primary printBtn" onclick="printDiv({{$vehicle->id}})" >Download or Print Card<div class="ripple-container"></div></button>
                                    </td>

                                  </tr>
                            @endforeach

                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>


        </div>

      </div>
    </div>

@endsection
@section("script")

<script type="text/javascript" src="https://jasonday.github.io/printThis/printThis.js"></script>

<script>

function printDiv(selector){

    $('.qrcode'+selector).printThis()
}
</script>

@endsection



