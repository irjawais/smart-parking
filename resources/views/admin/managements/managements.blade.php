@extends('layouts.app_dashboard_admin')

@section("css")
    <style>
        table tr td:nth-child(5) {
            text-align: right;
        }
        .paginate_button {
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
              {{--   <div class=""> --}}
                   {{--  <div class="clearfix"></div> --}}
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <!-- <h2>Payment Getway </h2> -->

                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                <div class="x_title">
                                <h2>Security User </h2>


                                <ul class="nav navbar-right panel_toolbox">

                                    <a class="btn btn-primary" href="{{route("admin.addManagement")}}">Add Security User</a>
                                   </ul>

                    <ul class="nav navbar-right panel_toolbox">

                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="table-responsive">
                                    <table id="table" class="table table-striped table-bordered">
                                            <thead>
                                                 <tr>
                                                    <td>Name</td>
                                                    <td>Email</td>
                                                    <td>Username</td>

                                                    <td>Created Date</td>

                                                    <td>Status</td>
                                                    <td>Action</td>


                                                    </tr>
                                                </thead>


                                    </table>
                                </div>
                            </div>
                        </div>
                        </div>

                    </div>
               {{--  </div> --}}


@endsection

@section("script")

<script type="text/javascript">
    $(document).ready(function () {

       var data_table = $('#table').DataTable({

       processing: true,
       serverSide: true,
       "iDisplayLength": 100,
       stateSave: true,
       "order": [[ 0, "desc" ]],
       ajax: {url:'{!! route('admin.ajaxManagements') !!}',
              data: function (d) {
              d.status = $('#filter-status').val();
              d.terminal = $('#filter-terminal').val();
              d.client = $('#filter-client').val();
              d.order_id = $('#order_id').val();

       }},
       columns: [
          { data: 'name', name: 'name' },
          { data: 'email', name: 'email' },
          { data: 'username', name: 'username' },
          { data: 'created_at', name: 'created_at' },

          {
                  data: 'is_active',
                  name: 'is_active',
                  "mRender": function (data) {
                      if(data== 1)
                          return `<span class="label label-success">Active</span>`;
                      else
                         return `<span class="label label-warning">Blocked</span>`;
                          return data;
                       }
           },
           {
                    "mData": null,
                    "bSortable": false,
                    "bSearchable": false,
                    stateSave: true,
                    "mRender": function(data) {
                        var edit = "{{ route('admin.editManagement',':number') }}"
                        edit = edit.replace(':number', data.id);

                        var block = "{{ route('admin.blockManagement',':number') }}"
                        block = block.replace(':number', data.id);
                        if(data.is_active==1)
                            return `
                            <div class="btn-group">
                            <a  class="btn dropdown-toggle" data-toggle="dropdown" data-disabled="true" aria-expanded="true">Options <span class="caret"></span></a>
                                <ul class="dropdown-menu dropdown-menu-left" role="menu" style="left:unset !important;right:0px;">
                                        <li><a title="Edit" href=${edit}><i class="fa fa-pencil"></i> Edit</a></li>
                                        <li><a title="Edit" href=${block}><i class="fa fa-trash"></i> Block</a></li>
                                </ul>
                            </div>`
                        else if(data.is_active==0)
                            return `
                            <div class="btn-group">
                            <a  class="btn dropdown-toggle" data-toggle="dropdown" data-disabled="true" aria-expanded="true">Options <span class="caret"></span></a>
                                <ul class="dropdown-menu dropdown-menu-left" role="menu" style="left:unset !important;right:0px;">
                                        <li><a title="Edit" href=${edit}><i class="fa fa-pencil"></i> Edit</a></li>
                                        <li><a title="Edit" href=${block}><i class="fa fa-trash"></i> Unblock</a></li>
                                </ul>
                            </div>`

                    }
                }

           ]
       });

       $('.filter').on('change', function (e) {

           data_table.draw();
       });
       $( ".filter" ).keyup(function() {
           data_table.draw();
       });
   });
   </script>
    <style>
        .current
        {
            background-color: #DDD !important;
        }
    </style>
@endsection
