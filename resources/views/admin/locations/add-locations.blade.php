@extends('layouts.app_dashboard_admin')
@section("css")
<style>
    table tr td:nth-child(5) {
        text-align: right;
    }

    .paginate_button {
        cursor: pointer;
    }
</style>
@endsection @section('content')
<div class="">
    <div class="clearfix"></div>

    <div class="row">
            @if ($errors->any())


            @foreach ($errors->all() as $error)
                    <div  class="alert alert-danger alert-dismissible fade in" role="alert">

                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                            <strong>  {{$error}}</strong>
                        </div>
            @endforeach

            @endif
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Locations <small>create parking location</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br>
                    <form id="management-form"  method="POST" action="{{route("admin.addLocationPost")}}" class="form-horizontal form-label-left" novalidate="">
                        @csrf
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Name <span class="required">*</span>
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="name" required="required" name="name" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group ">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Paid Type
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="radio"  id="is_paid" value="1"  name="is_paid" required class="is_paid form-control col-md-7 col-xs-12">

                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Free Type
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="radio"  id="is_paid" value="0"  name="is_paid" required  class="is_paid form-control col-md-7 col-xs-12">

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Price/slot <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="price"  name="price" placeholder="Leave empty if free type location" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="item form-group ">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Vehicle Types
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="vehicle_type_id" id="vehicle_type_id" class="form-control" required>
                            <option value="">Select</option>
                            @foreach ($vehicle_types as $vehicle_type)
                                <option  value="{{$vehicle_type->id}}" >{{$vehicle_type->name}}</option>

                            @endforeach
                            </select>
                            </div>
                        </div>
                        {{-- <div class="item form-group ">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">User Type
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="user_type_id" id="user_type_id" class="form-control" required>
                            <option value="">Select</option>
                            @foreach ($user_types as $user_type)
                                <option  value="{{$user_type->id}}" >{{$user_type->name}}</option>

                            @endforeach
                            </select>
                            </div>
                        </div> --}}
                        {{-- <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Number of slots <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="email" required="required" name="total_slots" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div> --}}



                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                {{-- <button class="btn btn-primary" type="button">Cancel</button> --}}
                                <button class="btn btn-primary" type="reset">Reset</button>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section("script")
<script src="{{ asset('js/bootstrap-validator/validator.min.js') }}"></script>
<script>

$('#management-form').validator()
$(document).ready(function(){
$(".is_paid").click(function() {

    var ischecked= $(this).val();
   if(ischecked==1){
       $("#price").prop('disabled', false);
   $("#price").attr("required", "true");


   }else{
   $("#price").prop('disabled', true);
   $("#price").removeAttr("required");

}
});
});
</script>
@endsection
