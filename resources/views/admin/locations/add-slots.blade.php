@extends('layouts.app_dashboard_admin')
@section("css")
<style>
    table tr td:nth-child(5) {
        text-align: right;
    }

    .paginate_button {
        cursor: pointer;
    }
</style>
@endsection @section('content')
<div class="">
    <div class="clearfix"></div>

    <div class="row">
            @if ($errors->any())


            @foreach ($errors->all() as $error)
                    <div  class="alert alert-danger alert-dismissible fade in" role="alert">

                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                            <strong>  {{$error}}</strong>
                        </div>
            @endforeach

            @endif
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Locations <small>{{$location->name}}</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br>
                    <form id="management-form"  method="POST" action="{{route("admin.addSlotPost")}}" class="form-horizontal form-label-left" novalidate="">
                        @csrf
                        <input type="hidden" name="location_id" value="{{$location->id}}">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Total Number Of Slots <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="name" required="required" name="number_of_slots" placeholder="Total Number Of Slots " min="0" pattern="[0-9]+" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Starting Slots No <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="name" required="required" name="starting_slots" placeholder="Starting Slots No" min="0" pattern="[0-9]+" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        {{-- <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Ending Slots No <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="name" required="required" name="ending_slots" placeholder="Ending Slots No " min="0" pattern="[0-9]+" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div> --}}





                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                {{-- <button class="btn btn-primary" type="button">Cancel</button> --}}
                                <button class="btn btn-primary" type="reset">Reset</button>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section("script")
<script src="{{ asset('js/bootstrap-validator/validator.min.js') }}"></script>
<script>

$('#management-form').validator()
$(document).ready(function(){
$(".is_paid").click(function() {

    var ischecked= $(this).val();
   if(ischecked==1){
       $("#price").prop('disabled', false);
   $("#price").attr("required", "true");


   }else{
    $("#price").prop('disabled', true);
    $("#price").removeAttr("required");

    }
});
});
</script>
@endsection
