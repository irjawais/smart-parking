<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Institute extends Authenticatable
{

    use Notifiable;
    protected $fillable = [
        'name', 'email', 'password','username','institute_id'
    ];

    protected $table = "institute";

}
