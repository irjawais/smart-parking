<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{


    protected $fillable = [
        'name',  'institute_id','admin_id','is_paid'
    ];
    protected $table = "user_type";
}
