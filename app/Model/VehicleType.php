<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class VehicleType extends Model
{


    protected $fillable = [
        'name',  'institute_id','admin_id'
    ];
    protected $table = "vehicle_type";

}
