<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Slot extends Model
{


    protected $fillable = [
        'institute_id','admin_id','slot_id','location_id','is_booked','is_block','is_occupied','name','lng','lat'
    ];

}
