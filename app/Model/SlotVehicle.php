<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class SlotVehicle extends Model
{


    protected $fillable = [
        'number_plate','base64image','slot_id','status','generated_id'
    ];
    protected $table = "slot_vehicle";
}
