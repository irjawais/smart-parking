<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{


    protected $fillable = [
        'vehicle_number',  'vehicle_type_id','institute_id','user_id'
    ];
    protected $table = "vehicles";

    public function parent(){
        return $this->hasOne( 'App\Model\VehicleType', 'id', 'vehicle_type_id' );
    }
}
