<?php

namespace App\Http\Controllers;

use App\Model\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    public function card()
    {
        return view('user.card');
    }
    public function plateSticker()
    {
        $vehicles = Vehicle::where('user_id',Auth::user()->id)->with('parent')->get();
        return view('user.plateSticker',compact('vehicles'));
    }

}
