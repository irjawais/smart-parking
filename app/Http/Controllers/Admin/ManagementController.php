<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Auth;
use DataTables;
use Illuminate\Support\Facades\Validator;
use App\Model\Management;
use App\Model\UserBalance;
use Illuminate\Support\Facades\Hash;
use Redirect;
use App\Model\VehicleType;
use App\Model\UserType;
use App\Model\Vehicle;
use App\User;

class ManagementController extends Controller
{

    public function managements()
    {

        return view('admin.managements.managements');
    }
    public function addManagement()
    {

        return view('admin.managements.add-management');
    }

    public function addManagementPost(Request $request){

        $validator =  Validator::make($request->all(),[
            'email' => 'required|unique:managements|min:7,email',
            'username' => 'required|unique:managements|min:4',
            'password' => 'required|min:6',
            'name' => 'required|min:3',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }



        Management::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'username' => $request->input('username'),
            'institute_id' => Auth::guard('admin')->user()->institute_id,
            'admin_id' => Auth::guard('admin')->user()->id,
            'password' => Hash::make($request->input('password')),
        ]);
        $request->session()->flash('message', 'User created successfully!');
        return Redirect::back();
    }
    public function ajaxManagements(Request $request){
        $management=Management::where('institute_id',Auth::guard('admin')->user()->institute_id);
        return DataTables::of($management)->make(true);
    }
    public function editManagement($id){
        $management=Management::find($id);
        return view('admin.managements.edit-management',compact('management'));
    }

    public function editManagementPost(Request $request){

        $validator =  Validator::make($request->all(),[
            'email' => 'required|min:7,email',
            'username' => 'required|min:4',

            'name' => 'required|min:3',
        ]);

        if($request->input('password')){
            $validator =  Validator::make($request->all(),[
               'password' => 'required|min:6',
            ]);
        }
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }



        $management = Management::where('id',$request->input('id'))->first();
        $management->email = $request->input('email');
        $management->name = $request->input('name');
        $management->username = $request->input('username');
        if($request->input('password')){
            $management->password = Hash::make($request->input('password'));

        }
        $management->save();

        $request->session()->flash('message', 'User : '.$management->name.' updated successfully!');
        return Redirect::back();
    }
    public function blockManagement($id,Request $request){
        $management = Management::where('id',$id)->first();
        if($management->is_active==1){
            $management->is_active=0;
        }else if($management->is_active==0){
            $management->is_active=1;
        }
        $management->save();
        $request->session()->flash('message', 'User : '.$management->name.' blocked!');

        return Redirect::back();

    }

    public function vehicleTypes()
    {
        return view('admin.managements.vehicle-types');
    }
    public function addVehicleType()
    {
        return view('admin.managements.add-vehicle-types');
    }
    public function addVehicleTypePost(Request $request){
        $vehicleType = VehicleType::create([
            'name' => $request->input('name'),

            'institute_id' => Auth::guard('admin')->user()->institute_id,
            'admin_id' => Auth::guard('admin')->user()->id,
        ]);
        $request->session()->flash('message', 'User : '.$vehicleType->name.' created successfully!');
        return Redirect::back();
    }
    public function ajaxVehicleType()
    {
        $management=VehicleType::where('institute_id',Auth::guard('admin')->user()->institute_id)->where('admin_id',Auth::guard('admin')->user()->id);
        return DataTables::of($management)->make(true);
    }
    public function usersTypes(){
        return view('admin.managements.user-types');
    }
    public function addUserType(){
        return view('admin.managements.add-user-types');
    }
    public function addUserTypePost(Request $request)
    {
        $vehicleType = UserType::create([
            'name' => $request->input('name'),
            'is_paid' => $request->input('is_paid'),
            'institute_id' => Auth::guard('admin')->user()->institute_id,
            'admin_id' => Auth::guard('admin')->user()->id,
        ]);
        $request->session()->flash('message', 'User : '.$vehicleType->name.' created successfully!');
        return Redirect::back();
    }
    public function ajaxUserType(Request $request){
        $management=UserType::where('institute_id',Auth::guard('admin')->user()->institute_id);
        return DataTables::of($management)->make(true);
    }
    public function users()
    {
        return view('admin.managements.users');
    }
    public function addUsers()
    {
        $vehicleTypes=VehicleType::select('name','id')->where('institute_id',Auth::guard('admin')->user()->institute_id)->get();
        $userTypes=UserType::select('name','id')->where('institute_id',Auth::guard('admin')->user()->institute_id)->get();
        return view('admin.managements.add-users',compact('vehicleTypes','userTypes'));
    }
    public function ajaxCheckUserType(Request $request){
        $user_type=UserType::where('id',$request->input('user_type_id'))->first();
        return $user_type->is_paid;
    }
    function generateCode($limit){
        $code = 0;
        for($i = 0; $i < $limit; $i++) { $code .= mt_rand(0, 9); }
        return $code;
    }
    public function addUserPost(Request $request){
        //dd($request->all());

        $user = new User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->user_type_id = $request->input('user_type_id');

        $user->password = Hash::make($request->input('password'));
        $user->password_string = $request->input('password');
        $user->generated_id = $this->generateCode(14);

        $user->username = $request->input('username');
        $user->institute_id =  Auth::guard('admin')->user()->institute_id;
        $user->save();

        if($request->input('balance')){
            $user->balance = $request->input('balance');
            $user->save();
            UserBalance::create([
                'amount'=>$user->balance,
                'user_id'=>$user->id,
                'is_deposit'=>1
            ]);
        }
        if($request->input('vehicle')){
            $array = $request->input('vehicle');
            $user->save();

            $newArray = array();
            foreach (array_keys($array) as $fieldKey) {
                foreach ($array[$fieldKey] as $key=>$value) {
                    $newArray[$key][$fieldKey] = $value;
                }
            }
            $vehicles = $newArray;
            foreach($vehicles as $vehicle){
                if(isset($vehicle['number']))
                Vehicle::create([
                    'vehicle_number'=>@$vehicle['number'],
                    'vehicle_type_id'=>@$vehicle['type_id'],
                    'institute_id'=> Auth::guard('admin')->user()->institute_id,
                    'user_id'=> $user->id,
                ]);

            }
        }
        $request->session()->flash('message', 'User : '.$user->name.' created successfully!');
        return Redirect::back();

    }
    public function ajaxUsers()
    {
        $management=User::select('users.*','user_type.is_paid','user_type.name as user_type_name')->where('users.institute_id',Auth::guard('admin')->user()->institute_id)
        ->join('user_type', 'user_type.id', '=', 'users.user_type_id');

        return DataTables::of($management)->make(true);
    }
    public function usersVehicles(Request $request)
    {
        $vehicles = Vehicle::where('user_id',$request->input('user'))->with('parent')->get();

        return $vehicles;
    }
    public function recharge($id){
        $user = User::find($id);
        return view('admin.managements.recharge',compact('user'));
    }
    public function rechargePost($id,Request $request){
        $user = User::find($id);
        $user_type=UserType::where('id',$user->user_type_id)->first();
        if($user_type->is_paid!=1)
        {
            $request->session()->flash('error', ' Recharge failed!');
            return redirect()->back();
        }
        $user->balance = ((float)$request->input('balance'))+((float)$user->balance);
        $user->save();
        UserBalance::create([
            'amount'=>$request->input('balance'),
            'user_id'=>$user->id,
            'is_deposit'=>1
        ]);
        $request->session()->flash('message', ' Recharge successfully!');
        return redirect()->back();
        return view('admin.managements.recharge',compact('user'));
    }


}
