<?php

namespace App\Http\Controllers\Management;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Model\Location;
use App\Model\Slot;
use App\Model\SlotVehicle;
use App\Model\UserBalance;
use App\Model\UserSlot;
use App\Model\UserType;
use App\Model\Vehicle;
use App\User;
use Auth;

class SensorController extends Controller
{
    public function sensorSlotApi($slot_id,$value){
        if($value==0){
            $slot = Slot::where('id',$slot_id)->where('is_booked',1)->first();
            if(!$slot)
                return array("status"=>"fail");
            $vehicle = SlotVehicle::where('slot_id',$slot->id)->where('status',1)->first();
            if(!$vehicle)
                return array("status"=>"fail");
            $vehicle->status = 0;
            $vehicle->save();
            $slot->is_occupied = $value;
            $slot->is_booked = 0;
            $slot->save();
            return array("status"=>"success","response"=>"Slot released!");
        }else{
            $slot = Slot::where('id',$slot_id)->where('is_booked',1)->first();
            if(!$slot)
                return array("status"=>"fail");
            $slot->is_occupied = $value;
            $slot->save();
            return array("status"=>"success","response"=>"Status : $slot->is_occupied");
        }
    }

}
