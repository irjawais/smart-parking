<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class StopIfUserBlocked
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guard('management')->user()->is_active==0){
            Auth::guard('management')->logout();

            abort(404,"Your account is blocked by Admin. Please contact your provider.");
        }
        return $next($request);
    }
}
