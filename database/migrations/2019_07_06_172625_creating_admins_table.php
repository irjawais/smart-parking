<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatingAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->bigIncrements('id');
            //$table->integer('institute_id')->unsigned()->index();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('username')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();


        });
        DB::statement('ALTER TABLE institute ENGINE = InnoDB');

        Schema::table('admins', function (Blueprint $table) {
            $table->bigInteger('institute_id')->unsigned()->index();

            $table->foreign('institute_id')->references('id')->on('institute');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /* Schema::table('admins', function(Blueprint $table)
        {
            $table->dropForeign('institute_id');

        }); */
        Schema::dropIfExists('institute_id');

        Schema::dropIfExists('admins');
        //
    }
}
