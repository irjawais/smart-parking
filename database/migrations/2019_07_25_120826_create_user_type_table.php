<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_type', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->boolean('is_paid')->default(0);
            $table->timestamps();
        });
        Schema::table('user_type', function (Blueprint $table) {
            $table->bigInteger('institute_id')->unsigned()->index();
            $table->bigInteger('admin_id')->unsigned()->index();

            $table->foreign('institute_id')->references('id')->on('institute');
            $table->foreign('admin_id')->references('id')->on('admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('institute_id');
        Schema::dropIfExists('admin_id');

        Schema::dropIfExists('user_type');
    }
}
