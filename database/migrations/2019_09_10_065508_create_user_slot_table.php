<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSlotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slot_vehicle', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string("number_plate")->nullable();
            $table->string("generated_id")->nullable();
            $table->longText("base64image")->nullable();

            $table->bigInteger('slot_id')->unsigned()->index();
            $table->boolean('status')->default(0)->comment("1 active, 0 losted");
            $table->foreign('slot_id')->references('id')->on('slots');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slot_vehicle');
    }
}
