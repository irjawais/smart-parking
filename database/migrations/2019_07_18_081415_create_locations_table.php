<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();

            $table->string('price')->nullable();

            $table->string('name')->nullable();
            $table->boolean('is_paid')->default(1);
            $table->bigInteger('number_of_slots')->nullable();
            $table->boolean('is_active')->default(1);

            $table->bigInteger('vehicle_type_id')->unsigned()->index();
            $table->foreign('vehicle_type_id')->references('id')->on('vehicle_type');

        });
        Schema::table('locations', function (Blueprint $table) {
            $table->bigInteger('institute_id')->unsigned()->index();
            $table->bigInteger('admin_id')->unsigned()->index();

            $table->foreign('institute_id')->references('id')->on('institute');
            $table->foreign('admin_id')->references('id')->on('admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institute_id');
        Schema::dropIfExists('admin_id');
        Schema::dropIfExists('locations');
    }
}
