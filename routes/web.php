<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/home', function () { return redirect()->route('home'); })->name("main");
Route::get('/loader', function () { return view('management.dashboard.loader'); })->name("loader");

Auth::routes();
Route::group(['middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/card', 'UserController@card')->name('card');
    Route::get('/plate-sticker', 'UserController@plateSticker')->name('plateSticker');

});

Route::group(['prefix' =>'admin','namespace'=>'Admin'], function () {
    // Admin Login Routes
    Route::get('/login','Auth\LoginController@showLoginForm')->name('admin.login');
    Route::get('/register','Auth\RegisterController@showRegistrationForm')->name('admin.register');
    Route::post('/register','Auth\RegisterController@register');
    Route::post('/login','Auth\LoginController@login');
    Route::post('/logout','Auth\LoginController@logout')->name('admin.logout');
    //Forgot Password Routes
    Route::get('/password/reset','Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('/password/email','Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    //Reset Password Routes
    Route::get('/password/reset/{token}','Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('/password/reset','Auth\ResetPasswordController@reset')->name('password.update');
});

Route::group(['prefix' =>'admin','namespace'=>'Admin','as' => 'admin.','middleware' => ['auth:admin']], function () {
    Route::get('/',function(){ return redirect()->route('admin.dashboard'); })->name('home');
    Route::get('/dashboard','HomeController@index')->name('dashboard');
    Route::get('/managements','ManagementController@managements')->name('managements');
    Route::get('/management/add','ManagementController@addManagement')->name('addManagement');
    Route::post('/add-management','ManagementController@addManagementPost')->name('addManagementPost');
    Route::get('/ajax-managements','ManagementController@ajaxManagements')->name('ajaxManagements');
    Route::get('/management/edit/{id}','ManagementController@editManagement')->name('editManagement');
    Route::post('/edit-management','ManagementController@editManagementPost')->name('editManagementPost');
    Route::get('/management/block/{id}','ManagementController@blockManagement')->name('blockManagement');

    Route::get('/vehicle/types','ManagementController@vehicleTypes')->name('vehicleTypes');
    Route::get('/vehicle/types/add','ManagementController@addVehicleType')->name('addVehicleType');
    Route::post('/vehicle/types/add','ManagementController@addVehicleTypePost')->name('addVehicleTypePost');
    Route::get('/ajax-vehicle/types/','ManagementController@ajaxVehicleType')->name('ajaxVehicleType');

    Route::get('/location','LocationController@location')->name('location');
    Route::get('/location/add','LocationController@addLocations')->name('addLocations');
    Route::post('/add-locations','LocationController@addLocationPost')->name('addLocationPost');
    Route::get('/ajax-locations','LocationController@ajaxLocation')->name('ajaxLocation');
    Route::get('/location/edit/{id}','LocationController@editLocation')->name('editLocation');
    Route::post('/edit-location/post','LocationController@editLocationPost')->name('editLocationPost');
    Route::get('/location/block/{id}','LocationController@blockLocation')->name('blockLocation');
    Route::get('/location/{id}','LocationController@viewLocation')->name('viewLocation');

    Route::get('/slots/{id}','LocationController@addSlots')->name('addSlots');
    Route::post('/location/add/post','LocationController@addSlotPost')->name('addSlotPost');
    Route::get('/ajax-slots','LocationController@ajaxSlots')->name('ajaxSlots');
    Route::get('/slots/block/{id}','LocationController@blockSlot')->name('blockSlot');

    Route::get('/slots/edit/{id}','LocationController@editSlot')->name('editSlot');
    Route::post('/slots/edit/post/{id}','LocationController@editSlotPost')->name('editSlotPost');


    Route::get('/user-type','ManagementController@usersTypes')->name('usersTypes');
    Route::get('/user-type/add','ManagementController@addUserType')->name('addUserType');
    Route::post('/user-type/add/post','ManagementController@addUserTypePost')->name('addUserTypePost');
    Route::get('/ajax/users/','ManagementController@ajaxUserType')->name('ajaxUserType');

    Route::get('/users','ManagementController@users')->name('users');
    Route::get('/users/add','ManagementController@addUsers')->name('addUsers');
    Route::post('/users/add/post','ManagementController@addUserPost')->name('addUserPost');
    Route::post('/ajax-check-user-type','ManagementController@ajaxCheckUserType')->name('ajaxCheckUserType');
    Route::get('/ajax-users','ManagementController@ajaxUsers')->name('ajaxUsers');
    Route::post('/users-vehicles','ManagementController@usersVehicles')->name('usersVehicles');
    Route::get('/recharge/{id}','ManagementController@recharge')->name('recharge');
    Route::post('/recharge/{id}/post','ManagementController@rechargePost')->name('rechargePost');









  });

  Route::group(['prefix' =>'management','namespace'=>'Management'], function () {
    // Admin Login Routes
    Route::get('/login','Auth\LoginController@showLoginForm')->name('management.login');
    Route::get('/register','Auth\RegisterController@showRegistrationForm')->name('management.register');
    Route::post('/register','Auth\RegisterController@register');
    Route::post('/login','Auth\LoginController@login');
    Route::post('/logout','Auth\LoginController@logout')->name('management.logout');
    //Forgot Password Routes
});

Route::group(['prefix' =>'management','namespace'=>'Management','middleware' => ['auth:management','stop_if_blocked']], function () {
    Route::get('/',function(){ return redirect()->route('management.dashboard'); })->name('management.home');
    Route::get('/dashboard','HomeController@index')->name('management.dashboard');
    Route::get('/check-point/in','ManagementController@checkPoint')->name('management.checkPoint');
    Route::post('/ajax-check-user','ManagementController@ajaxVerifyUser')->name('management.ajaxVerifyUser');
    Route::get('/check-point/out','ManagementController@checkPointOut')->name('management.checkPointOut');
    Route::post('/ajax-check-point/out','ManagementController@ajaxVerifyUserOut')->name('management.ajaxVerifyUserOut');

    //sensor api for enternce
    Route::get('/entry-point/{vehicle_type}','ManagementController@entryPoint')->name('management.entryPoint');
    Route::get('/log-vehicle','ManagementController@logVehicle')->name('logVehicle');

    Route::post('/ajax-save-vehicle','ManagementController@ajaxSaveVehicle')->name('management.ajaxSaveVehicle');



  });
  //sensor api for slots
  //{slot id}, value that u want to set there
  //http://127.0.0.1:8000/sensor-slot-api/1/1
  //http://127.0.0.1:8000/sensor-slot-api/1/0 //used at the time of exiting parking
  //sensor api url for entence {vehhicle type id}
  //http://127.0.0.1:8000/management/entry-point/1
  Route::get('/sensor-slot-api/{id}/{value}','Management\SensorController@sensorSlotApi')->name('sensorSlotApi');
//sudo php artisan serve --host 192.168.8.103 --port 80
